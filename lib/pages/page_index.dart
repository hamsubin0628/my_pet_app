import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          '말썽쟁이 함시우',
        style: TextStyle(
          letterSpacing: -1.5,
          fontSize: 25,
          fontWeight: FontWeight.w500,
          color: Colors.white,
        ),
        ),
        backgroundColor: Colors.blueAccent,
        foregroundColor: Colors.white,
      ),
      drawer: Drawer(
        child: ListView(
          children: [
        UserAccountsDrawerHeader(
        currentAccountPicture: CircleAvatar(
        backgroundImage: AssetImage('assets/04.jpeg'),
      ),
      accountName: Text(
          '함시우',
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w400,
        ),
      ),
      accountEmail: Text(
          '2020.09.07',
        style: TextStyle(
          fontSize: 13,
          fontWeight: FontWeight.w100,
        ),
      ),
          decoration: BoxDecoration(
            color: Colors.blueAccent,
          ),
    ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(
                'assets/01.jpeg',
              width:410,
              height: 510,
              fit: BoxFit.cover,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 30, 0, 50),
              child: Column(
                children: [
                  Text(
                      '☆★  시우를 소개합니다  ★☆',
                    style: TextStyle(
                      letterSpacing: -1.5,
                      fontSize: 25,
                      height: 3.0,
                      color: Colors.blueAccent,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Text(
                      '시우는 귀여워요. 하지만 말을 안들어요  : (',
                    style: TextStyle(
                      letterSpacing: -1.5,
                      fontSize: 18,
                      height: 1.5,
                      color: Colors.black54,
                      fontWeight: FontWeight.w100,
                    ),
                  ),
                  Text(
                    '어떻게 하면 말을 잘 들을까요 ?',
                    style: TextStyle(
                      letterSpacing: -1.5,
                      fontSize: 18,
                      height: 1.5,
                      color: Colors.black54,
                      fontWeight: FontWeight.w100,
                    ),
                  ),
                ],
              ),
            ),
        Center(),
        Container(
          margin: EdgeInsets.fromLTRB(4, 0, 0, 0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset(
                          'assets/02.jpeg',
                        width:202,
                        height: 202,
                        fit: BoxFit.cover,
                      ),
                      Image.asset(
                          'assets/03.jpeg',
                        width:202,
                        height: 202,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Image.asset(
                          'assets/04.jpeg',
                        width:202,
                        height: 202,
                        fit: BoxFit.cover,
                      ),
                      Image.asset(
                          'assets/05.jpeg',
                        width:202,
                        height: 202,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
